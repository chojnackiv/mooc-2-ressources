## Analyser une activité récupérée : Turtle 

Vous trouver sur le Web l'[**activité Turtle**](https://tribu.phm.education.gouv.fr/portal/share/exercices-turtle). 
Après l'avoir lu, J'ai trouvé : 

- Objectifs : 
  - Travail sur la boucle for. 
  - Travail sur les fonctions
  - Mise en pratique visuelle. 'Je vois ce que je fais'

- pré-requis à cette activité
  - Le principe d'écriture d'une boucle foret d'une fonction pour donner toute l'autonomie. Sinon on doit aider.

- Durée de l'activité
  - deux séances de 2h  pour un niveau 2nd
- Exercices cibles
  - Je ne comprends pas la question. 
  - Les exercices sont de difficulté croissante. 
- Description du déroulement de l'activité 
  - TP en autonomie. Le prof aide les élèves en dificulté et fait un point d'information à chaque changment de niveau.
- Anticipation des difficultés des élèves 
  - La visualisation des figures permet de s'auto-corriger.
  - Exercices progressifs avec des variantes qui permettent de faire un exercice quand on a eu une aide sur le premier.
  - Gestion de l'IDE python à voir : utiliser le bloc d'instruction et pas le shell. 
- Gestion de l'hétérogénéïté
  - Oui. Exercices progressifs.
  - On n'est pas obligé de finir tous les exercices de la fiche.

Voilà pour mon analyse rapide de la séance.

